#!/bin/sh

drawable="../android/res/drawable-"

for x in l m h xh 
do
  mkdir -p ${drawable}${x}dpi
done

convert -resize 18x18 notification.png ${drawable}ldpi/ic_notification.png
convert -resize 24x24 notification.png ${drawable}mdpi/ic_notification.png
convert -resize 36x36 notification.png ${drawable}hdpi/ic_notification.png
convert -resize 48x48 notification.png ${drawable}xhdpi/ic_notification.png
