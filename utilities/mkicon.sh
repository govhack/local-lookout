#!/bin/sh

drawable="../android/res/drawable-"

for x in l m h xh 
do
  mkdir -p ${drawable}${x}dpi
done

convert -resize 36x36 icon.png ${drawable}ldpi/ic_launcher.png
convert -resize 48x48 icon.png ${drawable}mdpi/ic_launcher.png
convert -resize 72x72 icon.png ${drawable}hdpi/ic_launcher.png
convert -resize 96x96 icon.png ${drawable}xhdpi/ic_launcher.png

convert -resize 512x512 icon.png store-icon.png
