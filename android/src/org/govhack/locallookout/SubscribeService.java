package org.govhack.locallookout;

import android.util.Log;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: rgarcia
 * Date: 1/06/13
 * Time: 2:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class SubscribeService {
    private static final String TAG = "LocalLookoutActivitySubscribeService";
//    private static final String SERVICE_URL = "http://59.167.222.122:8080/local-lookout-server/subscribe";
    private static final String SERVICE_URL = "http://local-lookout.appspot.com/subscribe";


    public static HttpResponse doPost(String url, JSONObject json) throws Exception
    {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost request = new HttpPost(url);
        StringEntity s = new StringEntity(json.toString());

        s.setContentEncoding("UTF-8");
        s.setContentType("application/json");

        request.setEntity(s);
        request.addHeader("accept", "application/json");

        return httpclient.execute(request);
    }

    public static void doBodyPost(String regid, String location, String categories) throws Exception
    {
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost request = new HttpPost(SERVICE_URL);

            StringBuilder sb = new StringBuilder();

            sb.append("regId=").append(regid).append("&");

            sb.append("geo=").append(location).append("&");

            sb.append("categories=").append(categories);

            String entityBody = sb.toString();
            Log.v(TAG, "body " + entityBody);

            StringEntity s = new StringEntity(entityBody);
            s.setContentEncoding("UTF-8");
            s.setContentType("application/x-www-form-urlencoded");

            request.setEntity(s);

            httpclient.execute(request);

        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}
