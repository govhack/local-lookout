package org.govhack.locallookout;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LocalLookoutActivity extends Activity {

    public static final String PROPERTY_REG_ID = "registration_id";
    GoogleCloudMessaging gcm;
    String GCM_SENDER_ID = "719750098134";
    private static final String TAG = "LocalLookoutActivity";
    String location = "";
    SharedPreferences prefs;
    String regid;


    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefs = getSharedPreferences(LocalLookoutActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);

        setContentView(R.layout.main);

        regid = prefs.getString(PROPERTY_REG_ID, null);

        // If there is no registration ID, the app isn't registered.
        // Call registerBackground() to registerWithApp it.
        if (regid == null) {
            Log.v(TAG, "No GCM registration Id so attempting to register in background");
            registerBackground();
        } else {
            Log.v(TAG, "Got GCM registration Id " + regid + " from properties");
        }

        gcm = GoogleCloudMessaging.getInstance(this);

        final Button saveButton = (Button) findViewById(R.id.saveButton);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v(TAG, "saveButton clicked");

                // sampleNotification(v.getContext());

                final EditText locationEdit = (EditText) findViewById(R.id.location);

                location = locationEdit.getText().toString();
                Log.v(TAG, "location " + location);

                // save checkboxes and location
                try {
                    subscribe();
                    Toast.makeText(getApplicationContext(), "Notification Settings Saved", 1).show();
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Notification Settings Failed!", 1).show();
                    Log.e(TAG, "Error in subscribe", e);
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }

            }
        });
    }

    public void registerWithApp() {

        try {
            RegisterService.post(regid);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


    }

    public void subscribe() throws Exception {

        JSONObject json = new JSONObject();

        // regid

        json.put("regid", regid);

        // Get location

        final String location = ((EditText) findViewById(R.id.location)).getText().toString();
        json.put("location", location);

        // Get checkbox values;

        CheckBox beaches = (CheckBox) findViewById(R.id.checkBox_beaches);
        CheckBox events = (CheckBox) findViewById(R.id.checkBox_events);
        CheckBox council = (CheckBox) findViewById(R.id.checkBox_council);
        CheckBox roads = (CheckBox) findViewById(R.id.checkBox_roads);
        CheckBox water = (CheckBox) findViewById(R.id.checkBox_water);
        CheckBox other = (CheckBox) findViewById(R.id.checkBox_other);


        final List<String> categories = new ArrayList<String>();

        if (beaches.isChecked()) {
            categories.add("beaches");
        }

        if (events.isChecked()) {
            categories.add("events");
        }

        if (council.isChecked()) {
            categories.add("council");
        }

        if (roads.isChecked()) {
            categories.add("roads");
        }

        if (water.isChecked()) {
            categories.add("water");
        }

        if (other.isChecked()) {
            categories.add("other");
        }


        JSONArray categoriesArray = new JSONArray(categories);
        json.put("categories", categoriesArray);


        new AsyncTask() {
            protected String doInBackground(Object... params) {
                String msg = "";

                try {
                    SubscribeService.doBodyPost(regid, location, categories.toString());
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    msg = "Error :" + e.getMessage();
                }

                return msg;
            }

            protected void onPostExecute(String msg) {
                Log.i(TAG, "Subscription task returned message " + msg);
            }
        }.execute(null, null, null);

    }

    private void registerBackground() {
        new AsyncTask() {
            protected String doInBackground(Object... params) {
                String msg = "";
                try {
                    regid = gcm.register(GCM_SENDER_ID);
                    msg = "Device registered, registration id=" + regid;
                    Log.v(TAG, msg);

                    // You should send the registration ID to your server over HTTP,
                    // so it can use GCM/HTTP or CCS to send messages to your app.

                    // For this demo: we don't need to send it because the device
                    // will send upstream messages to a server that will echo back
                    // the message using the 'from' address in the message.

                    // Save the regid for future use - no need to registerWithApp again.
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString(PROPERTY_REG_ID, regid);
                    editor.commit();

                    registerWithApp();

                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    Log.v(TAG, "REGISTER ERROR:  " + ex.getMessage());
                }
                return msg;
            }

            protected void onPostExecute(String msg) {
                Log.i(TAG, "Registration task returned message " + msg);
            }
        }.execute(null, null, null);
    }

    private void sampleNotification(Context ctx) {

        final String msg = "Lalalalalallla";
        final int NOTIFICATION_ID = 1;

        NotificationManager notificationManager = (NotificationManager)
                ctx.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = PendingIntent.getActivity(ctx, 0,
                new Intent(ctx, LocalLookoutActivity.class), 0);

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(ctx)
                        .setSmallIcon(R.drawable.ic_notification)
                        .setContentTitle("Local Lookout")
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setContentText(msg);

        builder.setContentIntent(contentIntent);
        notificationManager.notify(NOTIFICATION_ID, builder.build());
    }
}
