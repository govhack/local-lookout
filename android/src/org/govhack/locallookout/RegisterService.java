package org.govhack.locallookout;

import android.util.Log;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: rgarcia
 * Date: 1/06/13
 * Time: 2:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class RegisterService {
    private static final String TAG = "LocalLookoutRegisterService";
    //    private static final String SERVICE_URL = "http://59.167.222.122:8080/local-lookout-server/subscribe";
    private static final String SERVICE_URL = "http://local-lookout.appspot.com/register";


    public static void post(String regid) throws Exception
    {
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost request = new HttpPost(SERVICE_URL);

            StringBuilder sb = new StringBuilder();

            sb.append("regId=").append(regid).append("&");


            String entityBody = sb.toString();
            Log.v(TAG, "body " + entityBody);

            StringEntity s = new StringEntity(entityBody);
            s.setContentEncoding("UTF-8");
            s.setContentType("application/x-www-form-urlencoded");

            request.setEntity(s);

            httpclient.execute(request);

        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}
