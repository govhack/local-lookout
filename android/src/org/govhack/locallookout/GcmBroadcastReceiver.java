package org.govhack.locallookout;


import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class GcmBroadcastReceiver extends BroadcastReceiver {
    static final String TAG = "LocalLookoutBroadcastReceiver";
    public static final int NOTIFICATION_ID = 1;
    public static final String NOTIFICATION_TITLE = "Local Lookout";
    Context ctx;

    @Override
    public void onReceive(Context context, Intent intent) {
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
        ctx = context;
        String messageType = gcm.getMessageType(intent);

        Log.v(TAG, "Got GCM message type " + messageType);

        if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
            sendNotification("Send error: " + intent.getExtras().toString(), intent,false);
        } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
            sendNotification("Deleted messages on server: " +
                    intent.getExtras().toString(),intent,false);
        } else {

            String json = intent.getExtras().toString();

            Log.v(TAG, "Message content:  " + json);

            try {

                String category = intent.getStringExtra("category");

                String start = intent.getStringExtra("start_time");
                String end = intent.getStringExtra("end_time");
                String msg = intent.getStringExtra("notification");

                sendNotification(start + " - " + end + " | " + category + " | " + msg, intent,true);

            } catch(Exception e){
                Log.v(TAG, "Error on notify " + e.getMessage());
            }

        }
        setResultCode(Activity.RESULT_OK);
    }

    // Put the GCM message into a notification and post it.
    private void sendNotification(String msg, Intent notificationIntent, boolean display ) {

        Log.v(TAG, "Received notification message " + msg);

        NotificationManager notificationManager = (NotificationManager)
                ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent intent = new Intent(ctx, NotificationDetailsActivity.class);

        if( display){
            intent.putExtra("display", 2);
            intent.putExtras(notificationIntent);
        }

        PendingIntent contentIntent = PendingIntent.getActivity(ctx, 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);


        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(ctx)
                        .setSmallIcon(R.drawable.ic_notification)
                        .setContentTitle(NOTIFICATION_TITLE)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setContentText(msg);

        builder.setContentIntent(contentIntent);
        notificationManager.notify(NOTIFICATION_ID, builder.build());

        sendAlertToPebble(NOTIFICATION_TITLE, msg);
    }

    private void sendAlertToPebble(String title, String message) {
        final Intent i = new Intent("com.getpebble.action.SEND_NOTIFICATION");

        final Map data = new HashMap();
        data.put("title", title);
        data.put("body", message);
        final JSONObject jsonData = new JSONObject(data);
        final String notificationData = new JSONArray().put(jsonData).toString();

        i.putExtra("messageType", "PEBBLE_ALERT");
        i.putExtra("sender", "Local Lookouts");
        i.putExtra("notificationData", notificationData);

        Log.d(TAG, "About to send a modal alert to Pebble: " + notificationData);
        ctx.sendBroadcast(i);
    }


}