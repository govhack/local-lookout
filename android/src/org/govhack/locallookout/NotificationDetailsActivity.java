package org.govhack.locallookout;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.TextView;

public class NotificationDetailsActivity extends Activity {
    private static final String TAG = "LocalLookoutNotificationDetailsActivity";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.details);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Log.v(TAG, "OnStart");

        TextView category = (TextView) findViewById(R.id.category);
        TextView street = (TextView) findViewById(R.id.street);
        TextView date_from = (TextView) findViewById(R.id.date_from);
        TextView date_to = (TextView) findViewById(R.id.date_to);
        TextView message = (TextView) findViewById(R.id.message);

        Intent intent = getIntent();

        category.setText(intent.getStringExtra("category"));
        street.setText(intent.getStringExtra("geo"));
        date_from.setText(intent.getStringExtra("start_time"));
        date_to.setText(intent.getStringExtra("end_time"));
        message.setText(intent.getStringExtra("notification"));

    }
}