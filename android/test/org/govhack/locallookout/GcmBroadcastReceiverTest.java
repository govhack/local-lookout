package org.govhack.locallookout;

import android.content.Intent;
import android.test.AndroidTestCase;

public class GcmBroadcastReceiverTest extends AndroidTestCase {

    private GcmBroadcastReceiver receiver;
    private TestContext context;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        receiver = new GcmBroadcastReceiver();
        context = new TestContext();
    }

    public void testStartActivity()
    {
        Intent intent = new Intent(Intent.ACTION_NEW_OUTGOING_CALL);
        intent.putExtra(Intent.EXTRA_PHONE_NUMBER, "01234567890");

        receiver.onReceive(context, intent);
        assertEquals(1, context.getReceivedIntents().size());
        assertNull(receiver.getResultData());

        Intent receivedIntent = context.getReceivedIntents().get(0);
        assertNull(receivedIntent.getAction());
        assertEquals("01234567890", receivedIntent.getStringExtra("phoneNum"));
        assertTrue((receivedIntent.getFlags() & Intent.FLAG_ACTIVITY_NEW_TASK) != 0);
    }

}
