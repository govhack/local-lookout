#!/usr/bin/env python

import urllib
import urlparse
import lxml.html
import datetime
import codecs

page = 1
gccc_base = 'http://www.goldcoast.qld.gov.au/'
gccc_site = 'http://www.goldcoast.qld.gov.au/alerts-notifications-7792.html'
query = '?pg='

connection = urllib.urlopen(gccc_site + query + str(page))
dom =  lxml.html.fromstring(connection.read())

lastPageElement = dom.xpath('//*[@class="paging"]/a[@title="Last Page"]')[0]
lastPage = lastPageElement.attrib['href'][-1:]

print "Pages found: " + lastPage

now = datetime.datetime.now()
save_location = 'alerts' + str(now.strftime("-%Y-%m-%d-%H%M")) + ".txt"

for page in range(1, int(lastPage)) :
    connection = urllib.urlopen(gccc_site + query + str(page))

    dom = lxml.html.fromstring(connection.read())
    outputFile = codecs.open(save_location, 'a+', 'utf-8')

    for result in dom.xpath('//div[starts-with(@class, "result docs")]'): 
        date = ''
        for sibling in result.itersiblings(preceding=True):
            if sibling.tag == 'h2':
                date = sibling.text
                break
        
        outputFile.write('title=' + result.find('h3').find('a').text + ';')
        outputFile.write('category=' + result.find('div[@class="result-detail"]')[0].find('strong').text + ';')
        outputFile.write('date=' + date + ';')
        outputFile.write('time='+ result.find('div[@class="result-text"]').find('p').text + ';')
        outputFile.write('region=' + result.find('div[@class="result-detail"]').findall(path='p')[1].text + ';')
        outputFile.write('detail=' + result.find('div[@class="result-text"]').findall(path='p')[1].text + ';')
        outputFile.write('url=' + gccc_base + result.find('h3').find('a').attrib['href'])
        outputFile.write('\n') 
         
    outputFile.close()
