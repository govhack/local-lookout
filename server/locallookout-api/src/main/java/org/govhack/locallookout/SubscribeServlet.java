package org.govhack.locallookout;

import com.google.appengine.labs.repackaged.com.google.common.collect.ImmutableList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.govhack.locallookout.Constants.*;

public class SubscribeServlet extends HttpServlet {
	private static final long serialVersionUID = -1177230236266352064L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String id = request.getParameter(ID_PARAM);
		if (id == null || id.trim().length() == 0) {
            handleError(response, HttpServletResponse.SC_BAD_REQUEST, "Parameter " + ID_PARAM + " not found");
        }
		String category = request.getParameter(CATEGORIES_PARAM);
        List<String> categories;
		if (category == null || removeBrackets(category).trim().length() == 0) {
		    categories = ImmutableList.copyOf(Constants.CATEGORIES);
		} else {
            categories = Arrays.asList(removeBrackets(category).split(SPLIT_CHAR));
            for (String cat : categories) {
                if (cat.equals(CATEGORY_ALL)) {
                    categories = ImmutableList.copyOf(Constants.CATEGORIES);
                    break;
                }
                if (! CATEGORIES.contains(cat)) {
                    handleError(response, HttpServletResponse.SC_BAD_REQUEST, "Category " + cat + " is invalid");
                }
            }
        }

        String geo = request.getParameter(LOCATION_PARAM);
        if (geo == null || geo.trim().length() == 0) {
            handleError(response, HttpServletResponse.SC_BAD_REQUEST, "Parameter " + LOCATION_PARAM + " not found");
        }
        List<String> geos = Arrays.asList(geo.split(SPLIT_CHAR));

		Data.subscribe(id, categories, geos);
		response.setStatus(HttpServletResponse.SC_OK);
	}

    private String removeBrackets (String str) {
        return str.substring(1, str.length() - 1);
    }
}
