package org.govhack.locallookout;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

import static org.govhack.locallookout.Constants.*;

public class UnsubscribeServlet extends HttpServlet {
    private static final long serialVersionUID = 8625477604131168842L;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        String id = request.getParameter(ID_PARAM);
        if (id == null || id.trim().length() == 0) {
            handleError(response, HttpServletResponse.SC_BAD_REQUEST, "Parameter " + ID_PARAM + " not found");
        }
        String category = request.getParameter(CATEGORIES_PARAM);
        if (category == null || category.trim().length() == 0) {
            handleError(response, HttpServletResponse.SC_BAD_REQUEST, "Parameter " + CATEGORIES_PARAM + " not found");
        }
        List<String> categories = Arrays.asList(category.split(","));

        String geo = request.getParameter(LOCATION_PARAM);
        if (geo == null || geo.trim().length() == 0) {
            handleError(response, HttpServletResponse.SC_BAD_REQUEST, "Parameter " + LOCATION_PARAM + " not found");
        }
        List<String> geos = Arrays.asList(geo.split(","));

        Data.unsubscribe(id, categories, geos);
        response.setStatus(HttpServletResponse.SC_OK);
    }

}
