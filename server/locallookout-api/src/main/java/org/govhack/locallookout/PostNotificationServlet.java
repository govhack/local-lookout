package org.govhack.locallookout;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import com.google.common.collect.Iterables;

import static com.google.appengine.labs.repackaged.com.google.common.collect.Lists.newArrayList;
import static org.govhack.locallookout.Constants.*;

public class PostNotificationServlet extends HttpServlet {
    private static final long serialVersionUID = -702689135506325882L;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        String category = request.getParameter(CATEGORY_PARAM);
        if (category == null || category.trim().length() == 0) {
            System.out.println("Parameter " + CATEGORY_PARAM + " not found");
            category = CATEGORY_ALL;
        }

        String geo = request.getParameter(LOCATION_PARAM);
        if (geo == null || geo.trim().length() == 0) {
            System.out.println("Parameter " + LOCATION_PARAM + " not found");
            geo = ALL_GEO;
        }

        String notification = request.getParameter(ALERT_PARAM);
        if (notification == null || notification.trim().length() == 0) {
            Constants.handleError(response, HttpServletResponse.SC_BAD_REQUEST, "Parameter " + ALERT_PARAM + " not found");
        }

        String startTime = request.getParameter(START_PARAM);
        if (startTime == null || startTime.trim().length() == 0) {
            Constants.handleError(response, HttpServletResponse.SC_BAD_REQUEST, "Parameter " + START_PARAM + " not found");
        }

        String endTime = request.getParameter(END_PARAM);
        if (endTime == null || endTime.trim().length() == 0) {
            Constants.handleError(response, HttpServletResponse.SC_BAD_REQUEST, "Parameter " + END_PARAM + " not found");
        }

        List<String> registeredDevices = Data.getDevices();
        if (registeredDevices == null || registeredDevices.size() == 0) {
            sendResponse(response, "<p>No devices registered</p>");
            return;
        }
        StringBuilder responseMsg = new StringBuilder();
        List<String> targetDevices = newArrayList(Iterables.filter(registeredDevices, new CategoryFilter(category, geo)));
        if (targetDevices.isEmpty()) {
            sendResponse(response, "<p>No registered devices match notification criteria</p>");
            return;
        }
        responseMsg.append(String.format("Sent to %s subscribers", targetDevices.size()));
        Queue queue = QueueFactory.getQueue(QUEUE_NAME);
        if (targetDevices.size() == 1) {
            String regId = targetDevices.get(0);
            queue.add(TaskOptions.Builder.withUrl("/send").param(SendMsgServlet.ID, regId)
                    .param(SendMsgServlet.CATEGORY, category)
                    .param(SendMsgServlet.GEO, geo)
                    .param(SendMsgServlet.START_TIME, startTime)
                    .param(SendMsgServlet.END_TIME, endTime)
                    .param(SendMsgServlet.NOTIFICATION, notification));
        } else {
            List<String> msgRecipients = new ArrayList<String>();
            for (String device : targetDevices) {
                msgRecipients.add(device);
            }

            if (!msgRecipients.isEmpty()) {
                String multicastKey = Data.storeList(msgRecipients);
                TaskOptions taskOptions = TaskOptions.Builder.withUrl("/send")
                        .param(SendMsgServlet.MULTICAST_KEY, multicastKey)
                        .param(SendMsgServlet.CATEGORY, category)
                        .param(SendMsgServlet.GEO, geo)
                        .param(SendMsgServlet.START_TIME, startTime)
                        .param(SendMsgServlet.END_TIME, endTime)
                        .param(SendMsgServlet.NOTIFICATION, notification)
                        .method(Method.POST);
                queue.add(taskOptions);
            }
        }
        sendResponse(response, responseMsg.toString());
    }

    private void sendResponse(HttpServletResponse response, String msg) {
        response.setContentType("text/html");
        try {
            PrintWriter out = response.getWriter();
            out.print(msg);
            response.setStatus(HttpServletResponse.SC_OK);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}