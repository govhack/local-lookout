package org.govhack.locallookout;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Shared constants and methods.
 */
public class Constants {

    public static final String ID_PARAM = "regId";
    public static final String CATEGORIES_PARAM = "categories";
    public static final String CATEGORY_PARAM = "category";
    public static final String LOCATION_PARAM = "geo";
    public static final String ALERT_PARAM = "notification";
    public static final String START_PARAM = "start";
    public static final String END_PARAM = "end";
    public static final String QUEUE_NAME = "locallookout";
    public static final String SPLIT_CHAR = ",";
    public static final String ALL_GEO = "all";

    public static final String CATEGORY_ALL = "all";
    public static final String CATEGORY_BEACHES = "beaches";
    public static final String CATEGORY_COUNCIL = "council";
    public static final String CATEGORY_OTHER = "other";
    public static final String CATEGORY_EVENTS = "events";
    public static final String CATEGORY_ROADS = "roads";
    public static final String CATEGORY_WATER = "water";
    public static final List<String> CATEGORIES = Arrays.asList(CATEGORY_BEACHES, CATEGORY_COUNCIL, CATEGORY_OTHER,
            CATEGORY_EVENTS, CATEGORY_ROADS, CATEGORY_WATER);

    public static void handleError(HttpServletResponse response, int statusCode, String message) throws ServletException {
        try {
            response.sendError(statusCode, message);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ServletException("Internal error");
        }
    }
}
