package org.govhack.locallookout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Transaction;

public class Data {
    /**
     * Simple API Access key copied from Google APIs Console.
     */
    public static final String API_KEY = "AIzaSyCNcqFXJ6S-EOCw-fm4imHIUywt9gAh2t4";
    private static final String DEVICE_TYPE = "Device";
    private static final String LIST_TYPE = "List";
    private static final String LIST_PROPERTY = "deviceIds";
    private static final String DEVICE_REG_ID_PROPERTY = "regId";
    private static final String DEVICE_SUBSCRIBED_CATEGORIES = "categories";
    private static final String DEVICE_SUBSCRIBED_GEOS = "geos";
    private static final int MULTICAST_SIZE = 1000;
    private static final FetchOptions DEFAULT_FETCH_OPTIONS = FetchOptions.Builder.withPrefetchSize(MULTICAST_SIZE)
            .chunkSize(MULTICAST_SIZE);

    private static final Logger logger = Logger.getLogger(Data.class.getName());
    private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

    private Data() {
        throw new UnsupportedOperationException();
    }

    /**
     * Subscribe device with given registration ID to the specified categories and geolocations.
     */
    @SuppressWarnings("unchecked")
    public static void subscribe(String registrationId, List<String> newCategories, List<String> newGeos) {
        Transaction txn = datastore.beginTransaction();
        try {
            Entity entity = findDeviceById(registrationId);
            if (entity != null) {
                entity.setProperty(DEVICE_SUBSCRIBED_CATEGORIES, addData((List<String>) entity.getProperty(DEVICE_SUBSCRIBED_CATEGORIES), newCategories));
                entity.setProperty(DEVICE_SUBSCRIBED_GEOS, addData((List<String>) entity.getProperty(DEVICE_SUBSCRIBED_GEOS), newGeos));
                datastore.put(entity);
                txn.commit();
            }
        } finally {
            if (txn.isActive()) {
                txn.rollback();
            }
        }
    }

    /**
     * Unsubscribe the device with given registration ID from the specified categories and geolocations.
     */
    @SuppressWarnings("unchecked")
    public static void unsubscribe(String registrationId, List<String> categoriesToRemove, List<String> geosToRemove) {
        Transaction txn = datastore.beginTransaction();
        try {
            Entity entity = findDeviceById(registrationId);
            if (entity != null) {
                removeData((List<String>) entity.getProperty(DEVICE_SUBSCRIBED_CATEGORIES), categoriesToRemove);
                removeData((List<String>) entity.getProperty(DEVICE_SUBSCRIBED_GEOS), geosToRemove);

                datastore.put(entity);
                txn.commit();
            }
        } finally {
            if (txn.isActive()) {
                txn.rollback();
            }
        }
    }

    private static List<String> addData(List<String> list, List<String> itemsToAdd) {
        if (list == null) {
            list = new ArrayList<String>();
        }
        for (String item : itemsToAdd) {
            if (!list.contains(item)) {
                list.add(item);
            }
        }
        return list;
    }

    private static List<String> removeData(List<String> list, List<String> itemsToRemove) {
        for (String item : itemsToRemove) {
            if (list.contains(item)) {
                list.remove(item);
            }
        }
        return list;
    }

    /**
     * Register device on the server.
     */
    public static void register(String registrationId) {
        Transaction txn = datastore.beginTransaction();
        try {
            Entity entity = findDeviceById(registrationId);
            if (entity != null) {
                entity.setProperty(DEVICE_SUBSCRIBED_CATEGORIES, new ArrayList<String>());
                entity.setProperty(DEVICE_SUBSCRIBED_GEOS, new ArrayList<String>());
                datastore.put(entity);
                txn.commit();
                return;
            }
            entity = new Entity(DEVICE_TYPE);
            entity.setProperty(DEVICE_REG_ID_PROPERTY, registrationId);
            entity.setProperty(DEVICE_SUBSCRIBED_CATEGORIES, new ArrayList<String>());
            entity.setProperty(DEVICE_SUBSCRIBED_GEOS, new ArrayList<String>());
            datastore.put(entity);
            txn.commit();
        } finally {
            if (txn.isActive()) {
                txn.rollback();
            }
        }
    }

    /**
     * Unregister device from the server.
     */
    public static void unregister(String registrationId) {
        Transaction txn = datastore.beginTransaction();
        try {
            Entity entity = findDeviceById(registrationId);
            if (entity == null) {
                logger.warning("Device " + registrationId + " already unregistered");
            } else {
                Key key = entity.getKey();
                datastore.delete(key);
            }
            txn.commit();
        } finally {
            if (txn.isActive()) {
                txn.rollback();
            }
        }
    }

    /**
     * Updates the registration id of a device.
     */
    public static void updateRegistration(String oldId, String newId) {
        Transaction txn = datastore.beginTransaction();
        try {
            Entity entity = findDeviceById(oldId);
            if (entity == null) {
                logger.warning("No device for ID " + oldId);
                return;
            }
            entity.setProperty(DEVICE_REG_ID_PROPERTY, newId);
            datastore.put(entity);
            txn.commit();
        } finally {
            if (txn.isActive()) {
                txn.rollback();
            }
        }
    }

    /**
     * Gets all devices registered on the server.
     */
    public static List<String> getDevices() {
        List<String> devices;
        Transaction txn = datastore.beginTransaction();
        try {
            Query query = new Query(DEVICE_TYPE);
            Iterable<Entity> entities = datastore.prepare(query).asIterable(DEFAULT_FETCH_OPTIONS);
            devices = new ArrayList<String>();
            for (Entity entity : entities) {
                String device = (String) entity.getProperty(DEVICE_REG_ID_PROPERTY);
                devices.add(device);
            }
            txn.commit();
        } finally {
            if (txn.isActive()) {
                txn.rollback();
            }
        }
        return devices;
    }

    /**
     * Gets the total number of registered devices.
     */
    public static int getDevicesCount() {
        Transaction txn = datastore.beginTransaction();
        try {
            Query query = new Query(DEVICE_TYPE).setKeysOnly();
            List<Entity> allKeys = datastore.prepare(query).asList(DEFAULT_FETCH_OPTIONS);
            int total = allKeys.size();
            txn.commit();
            return total;
        } finally {
            if (txn.isActive()) {
                txn.rollback();
            }
        }
    }

    /**
     * Finds device with specified registration ID.
     */
    protected static Entity findDeviceById(String registrationId) {
        Filter deviceFilter = new FilterPredicate(DEVICE_REG_ID_PROPERTY, FilterOperator.EQUAL, registrationId);
        Query query = new Query(DEVICE_TYPE).setFilter(deviceFilter);
        PreparedQuery preparedQuery = datastore.prepare(query);
        List<Entity> entities = preparedQuery.asList(DEFAULT_FETCH_OPTIONS);
        Entity entity = null;
        if (!entities.isEmpty()) {
            entity = entities.get(0);
        }
        return entity;
    }

    protected static List<String> getCategories(String registrationId) {
        Entity device = findDeviceById(registrationId);
        if (device != null) {
            @SuppressWarnings("unchecked")
            List<String> categories = (List<String>) device.getProperty(DEVICE_SUBSCRIBED_CATEGORIES);
            return categories;
        }
        return null;
    }

    protected static List<String> getGeolocations(String registrationId) {
        Entity device = findDeviceById(registrationId);
        if (device != null) {
            @SuppressWarnings("unchecked")
            List<String> geos = (List<String>) device.getProperty(DEVICE_SUBSCRIBED_GEOS);
            return geos;
        }
        return null;
    }

    protected static String storeList(List<String> devices) {
        String encodedKey;
        Transaction txn = datastore.beginTransaction();
        try {
            Entity entity = new Entity(LIST_TYPE);
            entity.setProperty(LIST_PROPERTY, devices);
            datastore.put(entity);
            Key key = entity.getKey();
            encodedKey = KeyFactory.keyToString(key);
            txn.commit();
        } finally {
            if (txn.isActive()) {
                txn.rollback();
            }
        }
        return encodedKey;
    }

    protected static List<String> getList(String encodedKey) {
        Key key = KeyFactory.stringToKey(encodedKey);
        Entity entity;
        Transaction txn = datastore.beginTransaction();
        try {
            entity = datastore.get(key);
            @SuppressWarnings("unchecked")
            List<String> devices = (List<String>) entity.getProperty(LIST_PROPERTY);
            txn.commit();
            return devices;
        } catch (EntityNotFoundException e) {
            return Collections.emptyList();
        } finally {
            if (txn.isActive()) {
                txn.rollback();
            }
        }
    }

    protected static void deleteList(String encodedKey) {
        Transaction txn = datastore.beginTransaction();
        try {
            Key key = KeyFactory.stringToKey(encodedKey);
            datastore.delete(key);
            txn.commit();
        } finally {
            if (txn.isActive()) {
                txn.rollback();
            }

        }
    }
}
