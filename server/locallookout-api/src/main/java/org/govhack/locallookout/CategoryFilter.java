package org.govhack.locallookout;

import com.google.common.base.Predicate;

import java.util.ArrayList;
import java.util.List;

import static org.govhack.locallookout.Constants.ALL_GEO;
import static org.govhack.locallookout.Constants.CATEGORY_ALL;

public class CategoryFilter implements Predicate<String> {

    final String category;
    final String geo;

    public CategoryFilter(String category, String geo) {
        this.category = category;
        this.geo = geo;
    }

    @Override
    public boolean apply(String regId) {
        List<String> categories = Data.getCategories(regId);
        List<String> geos = Data.getGeolocations(regId);
        if (geo.equals(ALL_GEO)) {
            if (geos == null) {
                geos = new ArrayList<String>();
            }
            geos.add(ALL_GEO);
        }
        if (category.equals(CATEGORY_ALL)){
            if (categories == null) {
                categories = new ArrayList<String>();
            }
            categories.add(CATEGORY_ALL);
        }
        if (regId == null || categories == null || geos == null) {
            return false;
        }
        return categories.contains(category) && geos.contains(geo);
    }
}
