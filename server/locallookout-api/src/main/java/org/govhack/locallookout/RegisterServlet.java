package org.govhack.locallookout;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.govhack.locallookout.Constants.ID_PARAM;
import static org.govhack.locallookout.Constants.handleError;

public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = -1599410136164815638L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String id = request.getParameter(ID_PARAM);
		if (id == null || id.trim().length() == 0) {
            handleError(response, HttpServletResponse.SC_BAD_REQUEST, "Parameter " + ID_PARAM + " not found");
		}

		Data.register(id);
		response.setStatus(HttpServletResponse.SC_OK);
	}

}
