package org.govhack.locallookout;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.govhack.locallookout.Constants.*;

public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 6201795051143215419L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();
		out.print("<html>");
		out.print("<head>");
		out.print("  <title>GCM Sample App</title>");
        out.print("<style>" +
                "body{font:100%/1.2em Tahoma, Helvetica, sans-serif}" +
                "</style>");
		out.print("</head><body>");
		int total = Data.getDevicesCount();
		if (total == 0) {
			out.print("<h2>No devices registered!</h2>");
		} else {
            out.print("<img src='img/gccc-logo-lrg.png'><br><p>&nbsp;</p><br>");
            out.print("<img src='img/gccc-navbar.png'><br><br>");
            out.print("<h1>Alerts &amp; Notifications</h1>");
            out.print("<form name='form' method='POST' action='postNotification'>");
            out.print("<p>Category: <select id=\"" + CATEGORY_PARAM + "\" name=\"" + CATEGORY_PARAM +
                    "\"><option value=\"" + CATEGORY_ALL + "\">All categories</option><option value=\"" +
                    CATEGORY_BEACHES + "\">Beaches &amp; waterways</option><option value=\"" +
                    CATEGORY_COUNCIL + "\">Council facilities</option><option value=\"" +
                    CATEGORY_OTHER + "\">Other</option><option value=\"" +
                    CATEGORY_EVENTS + "\">Public events</option><option value=\"" +
                    CATEGORY_ROADS + "\">Roads</option><option value=\"" +
                    CATEGORY_WATER + "\">Water &amp; Wastewater</option></select></p>");
            out.print("<p>Location: <input type='text' placeholder='Enter street name or suburb' name='" + LOCATION_PARAM + "'/></p>");
            out.print("<p>Start Time: <input type='text' placeholder='HH:MM:DD/MM/YYYY' name='" + START_PARAM + "' size='16' maxlength='16'/></p>");
            out.print("<p>End Time: <input type='text' placeholder='HH:MM:DD/MM/YYYY' name='" + END_PARAM + "' size='16' maxlength='16'/></p>");
            out.print("Message: <p><textarea rows='5' cols='50' placeholder='Enter message' name='" + ALERT_PARAM + "'></textarea></p>");
            out.print("<input type='submit' value='Send Notification' />");
            out.print("</form>");
            out.print(String.format("<p>%s Devices have registered for notifications.</p>",total));
        }
		out.print("</body></html>");
		resp.setStatus(HttpServletResponse.SC_OK);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		doGet(req, resp);
	}

}
