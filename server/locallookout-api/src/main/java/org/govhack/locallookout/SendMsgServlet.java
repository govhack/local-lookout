package org.govhack.locallookout;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.android.gcm.server.Constants;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

public class SendMsgServlet extends HttpServlet {
    private static final long serialVersionUID = 598633126425445663L;
    public static final String ID = "regId";
    public static final String MULTICAST_KEY = "multicast";
    public static final String CATEGORY = "category";
    public static final String GEO = "geo";
    public static final String START_TIME = "start_time";
    public static final String END_TIME = "end_time";
    public static final String NOTIFICATION = "notification";
    /**
     * Sender used to send messages to GCM server.
     */
    private final Sender sender = new Sender(Data.API_KEY);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String registrationId = request.getParameter(ID);
        if (registrationId != null) {
            sendSingle(registrationId, request, response);
            return;
        }

        String multicastKey = request.getParameter(MULTICAST_KEY);
        if (multicastKey != null) {
            sendMulticast(multicastKey, request, response);
            return;
        }

        taskDone(response);
        return;
    }

    private void sendSingle(String registrationId, HttpServletRequest request, HttpServletResponse response) {
        Message msg = new Message.Builder()
                .addData(CATEGORY, request.getParameter(CATEGORY))
                .addData(GEO, request.getParameter(GEO))
                .addData(START_TIME, request.getParameter(START_TIME))
                .addData(END_TIME, request.getParameter(END_TIME))
                .addData(NOTIFICATION, request.getParameter(NOTIFICATION)).build();
        Result result = null;
        try {
            result = sender.sendNoRetry(msg, registrationId);
        } catch (IOException e) {
            taskDone(response);
        }

        if (result != null) {
            if (result.getErrorCodeName().equals(Constants.ERROR_NOT_REGISTERED)) {
                Data.unregister(registrationId);
            }
        }
    }

    private void sendMulticast(String multicastKey, HttpServletRequest request, HttpServletResponse response) {
        List<String> multicastIds = Data.getList(multicastKey);
        Message msg = new Message.Builder()
                .addData(CATEGORY, request.getParameter(CATEGORY))
                .addData(GEO, request.getParameter(GEO))
                .addData(START_TIME, request.getParameter(START_TIME))
                .addData(END_TIME, request.getParameter(END_TIME))
                .addData(NOTIFICATION, request.getParameter(NOTIFICATION)).build();
        try {
            sender.sendNoRetry(msg, multicastIds);
        } catch (IOException e) {
            Data.deleteList(multicastKey);
            taskDone(response);
        }
    }

    private void taskDone(HttpServletResponse response) {
        response.setStatus(200);
    }
}
