package org.govhack.locallookout;


import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import com.google.common.collect.Iterables;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.List;

import static com.google.appengine.labs.repackaged.com.google.common.collect.Lists.newArrayList;
import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;


@RunWith(PowerMockRunner.class)
@PrepareForTest({Data.class} )
public class CategoryFilterTest {

    @Before
    public void setUp(){
        mockStatic(Data.class, (Class[])null);
    }

    @Test
    public void runDev1(){

        when(Data.getGeolocations("1")).thenReturn(Lists.<String>newArrayList("lb"));
        when(Data.getCategories("1")).thenReturn(Lists.<String>newArrayList("ca", "cb"));

        List<String> targetDevices = newArrayList(Iterables.filter(Lists.newArrayList("2","1"), new CategoryFilter("ca", "lb")));
        assertEquals(1, targetDevices.size());

    }
    @Test
    public void runDev2(){

        when(Data.getGeolocations("2")).thenReturn(Lists.<String>newArrayList("la"));
        when(Data.getCategories("2")).thenReturn(Lists.<String>newArrayList("cb"));

        List<String> targetDevices = newArrayList(Iterables.filter(Lists.newArrayList("2","1"), new CategoryFilter("cb", "la")));
        assertEquals(1, targetDevices.size());

    }
    @Test
    public void runDev1_and_Dev2_NoCategory(){

        when(Data.getGeolocations("1")).thenReturn(Lists.<String>newArrayList("lb"));
        when(Data.getCategories("1")).thenReturn(Lists.<String>newArrayList());
        when(Data.getGeolocations("2")).thenReturn(Lists.<String>newArrayList("lb"));
        when(Data.getCategories("2")).thenReturn(null);

        List<String> targetDevices = newArrayList(Iterables.filter(Lists.newArrayList("2","1"), new CategoryFilter(Constants.CATEGORY_ALL, "lb")));
        assertEquals(2, targetDevices.size());

    }
    @Test
    public void runDev1_and_Dev2_AllLocations(){

        when(Data.getGeolocations("1")).thenReturn(Lists.<String>newArrayList("lb"));
        when(Data.getCategories("1")).thenReturn(Lists.<String>newArrayList("cb"));
        when(Data.getGeolocations("2")).thenReturn(Lists.<String>newArrayList("la"));
        when(Data.getCategories("2")).thenReturn(Lists.<String>newArrayList("cb"));

        List<String> targetDevices = newArrayList(Iterables.filter(Lists.newArrayList("2","1"), new CategoryFilter("cb", Constants.ALL_GEO)));
        assertEquals(2, targetDevices.size());

    }
    @Test
    public void runDev1noLoc_and_Dev2_AllLocations(){

        when(Data.getGeolocations("1")).thenReturn(null);
        when(Data.getCategories("1")).thenReturn(Lists.<String>newArrayList("cb"));
        when(Data.getGeolocations("2")).thenReturn(Lists.<String>newArrayList("la"));
        when(Data.getCategories("2")).thenReturn(Lists.<String>newArrayList("cb"));

        List<String> targetDevices = newArrayList(Iterables.filter(Lists.newArrayList("2","1"), new CategoryFilter("cb", Constants.ALL_GEO)));
        assertEquals(2, targetDevices.size());

    }


}
